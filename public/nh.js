function FetchAPI(address, password) {
	if (!address.startsWith("http")) {
		address = `http://${address}`
	}

	let hashSend = ""

	if (password) {
		hashSend = hex_sha512(password)
		console.log(hashSend)
	}

	var request = new XMLHttpRequest()
	request.onreadystatechange = (function() {
		if (this.readyState == 4) {
			if (this.status == 200) {
				document.getElementById("address").setAttribute("class", "input is-large is-success")
				localStorage.setItem("address", address)
				if (password)
					localStorage.setItem("sha512", password)

				appendToHTML(this.responseText, address, undefined)
			} else if (this.status == 401) {
				document.getElementById("address").setAttribute("class", "input is-large is-success")

				document.getElementById("password").setAttribute("class", "input is-large is-danger")
				document.getElementById("password").value = "Invalid Password!"
			} else {
				document.getElementById("address").setAttribute("class", "input is-large is-danger")
			}
		}
	})

	request.open("GET", address)
	if (password) {
		request.setRequestHeader("Hash", hashSend)
		console.log(hashSend)
	}

	request.send()
}

function parseError(json) {
	json = JSON.parse(json)

	if (json.status) {
		return json.error
	} else {
		return false
	}
}

function initPage() {
	address = localStorage.getItem("address")
	if (!address) 
		return

	password = localStorage.getItem("sha512")

	FetchAPI(address, password)
}

function appendFromLocalStorage(keyword) {
	json = sessionStorage.getItem("json")

	address = localStorage.getItem("address")
	if (!address || !json)
		location.reload()

	if (keyword !== undefined) {
		appendToHTML(json, address, keyword)
	} else {
		appendToHTML(json, address, undefined)
	}

}

function logOut() {
	localStorage.removeItem("address")
	localStorage.removeItem("sha512")
	sessionStorage.removeItem("json")

	location.reload()
}

function handleInput() {
	address  = document.getElementById("address").value
	password = document.getElementById("password").value

	if (!address) {
		document.getElementById("address").setAttribute("class", "input is-large is-danger")
		return
	}

	FetchAPI(address, password)
}

function makeBar(name) {
	let navbar = document.createElement("navbar")
		navbar.setAttribute("class", "navbar is-mobile")

	let navbarMenu = document.createElement("div")
		navbarMenu.setAttribute("class", "navbar-brand")

	if (name) {
		let navbarStart = document.createElement("div")
			navbarStart.setAttribute("class", "navbar-start")

		let backButton = document.createElement("a")
			backButton.setAttribute("class", "navbar-item back")
			backButton.setAttribute("onclick", "back()")
			backButton.setAttribute("href", "#")
			backButton.textContent = "Back"

			navbarStart.appendChild(backButton)

		let infoText = document.createElement("p")
			infoText.setAttribute("class", "navbar-item name")
			infoText.textContent = name

			navbarStart.appendChild(infoText)

		navbarMenu.appendChild(navbarStart)
	} 
	else {
		let navbarStart = document.createElement("div")
			navbarStart.setAttribute("class", "navbar-start")

		let refreshButton = document.createElement("a")
			refreshButton.setAttribute("class", "navbar-item refresh")
			refreshButton.setAttribute("onclick", "location.reload()")
			refreshButton.setAttribute("href", "#")
			refreshButton.textContent = "Refresh"

			navbarStart.appendChild(refreshButton)

		let searchBar = document.createElement("input")
			searchBar.setAttribute("type", "text")
			searchBar.setAttribute("placeholder", "Filter...")
			searchBar.setAttribute("class", "input")
			searchBar.setAttribute("id", "filter")
			searchBar.setAttribute("onChange", "appendFromLocalStorage(this.value)")

			navbarStart.appendChild(searchBar)

		let navbarEnd = document.createElement("div")
			navbarEnd.setAttribute("class", "navbar-end")

		let logoutButton = document.createElement("a")
			logoutButton.setAttribute("class", "navbar-item log-out")
			logoutButton.setAttribute("onclick", "logOut()")
			logoutButton.setAttribute("href", "#")
			logoutButton.textContent = "Log Out"	

			navbarEnd.appendChild(logoutButton)

		navbarMenu.appendChild(navbarStart)
		navbarMenu.appendChild(navbarEnd)
	}

	navbar.appendChild(navbarMenu)

	return navbar
}

// third arg acts as a search function
function appendToHTML(rawjson, address, keyword) {
	var url = new URL(address)

	let hero = document.createElement("section")
	if (keyword == undefined) {
		sessionStorage.setItem("json", rawjson)

		document.body.innerHTML = ""
		document.body.appendChild(makeBar()) // Add in Top Bar

		hero.setAttribute("class", "hero")
		hero.setAttribute("id", "hero")
	} else {
		document.getElementById("hero").innerHTML = ""
		hero = document.getElementById("hero")
	}

	let randomizer = document.createElement("div")
		randomizer.setAttribute("id", "randomizer")
		randomizer.setAttribute("onclick", "randomize()")
		randomizer.setAttribute("href", "#")
		randomizer.innerHTML = ""

		hero.appendChild(randomizer)
	
	let herobody = document.createElement("div")
		herobody.setAttribute("class", "hero-body")

	window.ea = null
	window.tilesAnc = null

	json = JSON.parse(rawjson)
	
	json.forEach(gallery => {
		if (keyword !== undefined) {
			if (!gallery.name.toUpperCase().includes(keyword.toUpperCase())) {
				return
			}
		}

		name = gallery.name + "/"
		pages = gallery.pages

		if (!name || !pages[0])
			return

		if (window.ea == 2 || window.ea === null) {	
			if (window.tilesAnc)
				herobody.appendChild(window.tilesAnc)

			window.tilesAnc = document.createElement("div")
			window.tilesAnc.setAttribute("class", "columns is-desktop") // Bulma's tiles

			window.ea = 0
		} else {
			window.ea++
		}

		let tile = document.createElement("div")
			tile.setAttribute("class", "column")

		let href = document.createElement("a")
			href.href = "#"
			href.setAttribute("onclick", `loadPage("${url.origin}", "${encodeURIComponent(name)}")`)

		let tileChild = document.createElement("div")
			tileChild.setAttribute("class", "gallery roundrect")
			tileChild.style.backgroundImage = `url("${url.origin}/img/${encodeURIComponent(name)}${encodeURIComponent(pages[0])}")`
			tileChild.style.backgroundSize = "cover"

		let div = document.createElement("p")
			div.setAttribute("class", "bottomOverlay")
			div.innerText = name.slice(0,-1)

		tileChild.appendChild(div)
		href.appendChild(tileChild)
		tile.appendChild(href)

		window.tilesAnc.appendChild(tile)
	})

	if (window.tilesAnc !== null) {
		for (i=0; i<(2-window.ea); i++) {
			let tile = document.createElement("div")
				tile.setAttribute("class", "column")

			window.tilesAnc.appendChild(tile)
		}

		herobody.appendChild(window.tilesAnc)
	} else {
		nope = document.createElement("div")
		nope.setAttribute("class", "title") // Bulma's tiles
		nope.innerText = "No results found."

		herobody.appendChild(nope)
	}

	if (keyword !== undefined) { 
		hero.appendChild(herobody)
	} else {
		hero.appendChild(herobody)
		document.body.appendChild(hero)
	}
}

function loadPage(origin, name) {
	console.log("Storing scroll at", getScroll())
	sessionStorage.setItem("scroll", getScroll())

	document.body.innerHTML = ""

	rawjson = sessionStorage.getItem("json")
	if (!rawjson) {
		return
	}

	galleries = JSON.parse(rawjson)
	galleries.forEach(gallery => {
		if (encodeURIComponent(gallery.name + "/") == name) {
			// First GoHome button
			//document.body.appendChild(closeDiv.cloneNode(true))
			name = gallery.name + "/"
			pages = gallery.pages

			document.body.appendChild(makeBar(name.slice(0, -1)))

			pages.forEach(page => {
				let tile = document.createElement("div")
					tile.setAttribute("class", "pages")

				let img = document.createElement("img")
					img.src = origin + "/img/" + encodeURIComponent(name) + encodeURIComponent(page)
					img.setAttribute("class", "page")

				tile.appendChild(img)
				document.body.appendChild(tile)
			})

			let footer = document.createElement("footer")
				footer.setAttribute("class", "footer has-text-centered")

			let bmuButton = document.createElement("button")
				bmuButton.setAttribute("class", "button is-primary is-rounded is-large is-fullwidth")
				bmuButton.setAttribute("onclick", "beamMeUp()")
				bmuButton.setAttribute("href", "#")
				bmuButton.textContent = "Back to Top"

			footer.appendChild(bmuButton)
			document.body.appendChild(footer)

			let sizeButton = document.createElement("div")
				sizeButton.setAttribute("id", "size")
				sizeButton.setAttribute("onclick", "toggleSize()")
				sizeButton.setAttribute("href", "#")
				sizeButton.innerHTML = ""
		
				document.body.appendChild(sizeButton)

			// Second GoHome button
			//document.body.appendChild(closeDiv.cloneNode(true))

			return
		}
	})
}

function randomize() {
	var limit = document.body.offsetHeight - window.innerHeight
	var rand = Math.random() * limit

	setScroll(Math.round(rand))
}

function beamMeUp() {
	setScroll(0)
}

function setScroll(scr) {
	document.documentElement.scrollTop = scr
	document.body.scrollTop = scr
}

function getScroll() {
	var scr = document.body.scrollTop
	if (!scr || scr == 0) {
		scr = document.documentElement.scrollTop
	}
	
	return scr
}

function back() {
	appendFromLocalStorage()
	// Restore scroll
	var oldscroll = sessionStorage.getItem("scroll")
	RestoreScroll(oldscroll)
}

function RestoreScroll(oldscroll) {
	setTimeout(function() {
		console.log("Setting to", sessionStorage.getItem("scroll"))
		setScroll(oldscroll)
		if (oldscroll != getScroll()) {
			RestoreScroll()
		}
	}, 20)
}

function toggleSize() {
	for (i=0; i<document.styleSheets[1].rules.length; i++) {
		if (document.styleSheets[1].rules[i].selectorText == "img.page") {
			console.log("Found element index", i)

			if (document.styleSheets[1].rules[i].style.maxHeight == "100vh") {
				document.styleSheets[1].rules[i].style.maxHeight = "100vw"
			} else {
				document.styleSheets[1].rules[i].style.maxHeight = "100vh"
			}

			console.log("After-value", document.styleSheets[1].rules[i].style.maxHeight)
		}
	}
}