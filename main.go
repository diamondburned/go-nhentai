package main

import (
	"bufio"
	"flag"
	"fmt"
	"gitlab.com/diamondburned/go-nhentai/nhentai" // "gitlab.com/diamondburned/go-nhentai/tui"
	"gitlab.com/diamondburned/go-nhentai/utils"
	"io/ioutil"
	"log"
	"os"
	"os/user"
	"runtime"
	"strings"
)

func main() {
	usr, _ := user.Current()
	config, err := NewConfig(usr.HomeDir+"/.config/go-nhentai/", "config.json")

	if err != nil {
		fmt.Println("Error encountered while trying to read config: ", err.Error())
		if yes, err := yOrN("Write default config? "); err == nil && yes {
			err := DefaultConfig().Write()
			if err != nil {
				fmt.Println("Failed to write config!")
			}
		}

		os.Exit(1)
	}

	silent, force, threads, assetsPath, args := ParseFlags()

	// Disable all fancy logging stuff
	log.SetFlags(0)
	// Sets the logger to no-op if the silent flag is set
	if silent {
		log.SetOutput(ioutil.Discard)
	}

	if len(args) == 0 {
		println("Error: You haven't selected any operation!")
		os.Exit(1)
	}

	switch args[0] {
	case "ws":
		(&DoujinServer{config.DownloadLocation, config.APIhash, config.APIendpoint}).Start(assetsPath)
	case "tui":
		println("Not Supported Yet!")
		// tui.Start()
	case "favorite":
		user := nhentai.NewUser(os.Getenv("USERNAME"), os.Getenv("PASSWORD"))
		user.Authenticate()
		results, _ := user.Favorites()
		for _, gallery := range results {
			GetGallery(gallery, config, force, threads)
		}
	case "latest", "update":
		update(config, force, threads)
	case "get", "gallery":
		for _, arg := range args[1:] {
			println(arg)
			gallery, err := nhentai.NewGalleryString(arg)
			if err != nil {
				utils.DebugPrint(0, "Failed to get gallery: "+arg+", because "+err.Error())
				continue
			}

			GetGallery(gallery, config, force, threads)
		}
	default:
		println("Invalid argument.")
		os.Exit(1)
	}
}

// ParseFlags parses all the flags
func ParseFlags() (bool, bool, int, string, []string) {
	var (
		silent     bool
		force      bool
		threads    int
		assetsPath string
	)

	flag.BoolVar(&silent, "silent", false, "Prints extra information")
	flag.BoolVar(&force, "force", false, "Force download even if a doujin is in the fetched list")
	flag.IntVar(&threads, "threads", runtime.NumCPU(), "Number of concurrent page downloads")
	flag.StringVar(&assetsPath, "assetspath", DefaultAssetsPath(), "The path to the server's assets")
	flag.Parse()

	return silent, force, threads, assetsPath, flag.Args()
}

// From: https://github.com/tsoding/snitch/blob/master/main.go
func yOrN(question string) (bool, error) {
	reader := bufio.NewReader(os.Stdin)

	fmt.Printf("%s [y/n] ", question)
	input, err := reader.ReadString('\n')
	text := strings.TrimSpace(input)

	for err == nil && text != "y" && text != "n" {
		fmt.Printf("%s [y/n] ", question)
		text, err = reader.ReadString('\n')
	}

	if err != nil || text == "n" {
		return false, err
	}

	return true, err
}
