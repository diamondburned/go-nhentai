# How to contribute

1. Fork
2. Modify
3. Request

## Guidelines

- Keep code tidy
- Lint them before merging
- Have comments
- Before merging, please check and update the CI file.