package nhentai

import (
	"crypto/tls"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/diamondburned/go-nhentai/utils"
)

// User represents an nhentai user
type User struct {
	sessionToken *http.Cookie
	username     string
	password     string
	numFavorites int
	favorites    []*Gallery
}

// NewUser returns a new (unauthenticated) user instance
func NewUser(username, password string) *User {
	return &User{password: password, username: username}
}

// Authenticated returns if the user is authenticated
func (user *User) Authenticated() bool {
	return user.sessionToken != nil
}

// Authenticate authenticates the user
func (user *User) Authenticate() error {
	endpoint := "https://nhentai.net/login/"

	client := &http.Client{
		Transport: &http.Transport{TLSClientConfig: &tls.Config{}},
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		}}
	req, err := http.NewRequest("GET", endpoint, nil)
	if err != nil {
		return err
	}
	req.Header = map[string][]string{"Origin": {"https://nhentai.net"}, "Referer": {endpoint}}

	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	match := regexp.MustCompile(`"csrfmiddlewaretoken" value="(.*)"`).FindStringSubmatch(string(respBody))
	if match == nil {
		return errors.New("couldn't get the middleware token")
	}

	req, err = http.NewRequest("POST", endpoint, strings.NewReader(url.Values{"next": nil, "password": {user.password}, "username_or_email": {user.username}, "csrfmiddlewaretoken": {match[1]}}.Encode()))
	req.Header = map[string][]string{
		"Content-Type": {"application/x-www-form-urlencoded"},
		"Origin":       {"https://nhentai.net"},
		"Referer":      {endpoint}}
	req.AddCookie(utils.GetCookie(resp.Cookies(), "csrftoken"))

	resp, err = client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	user.sessionToken = utils.GetCookie(resp.Cookies(), "sessionid")

	return nil
}

// Favorites returns a slice of the user's favorites
// TODO(ym): Refactor
func (user *User) Favorites() ([]*Gallery, error) {
	if user.sessionToken == nil && user.favorites == nil {
		return nil, errors.New("user is not authenticated")
	}
	if user.numFavorites < 1 && user.favorites == nil{
		regex := regexp.MustCompile(`<h1>My Favorites <span class="count">\((.*)\)</span></h1>`)

		req, err := http.NewRequest("GET", "https://nhentai.net/favorites/", nil)
		req.AddCookie(user.sessionToken)
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			return nil, err
		}
		defer resp.Body.Close()
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}
		num, _ := strconv.ParseInt(strings.Replace(regex.FindStringSubmatch(string(body))[1], ",", "", -1), 0, 64)
		user.numFavorites = int(num)
	} else if user.favorites != nil {
		user.numFavorites = len(user.favorites)
	}

	if user.favorites == nil {
		channel, _, _ := realSearch("https://nhentai.net/favorites/?q=", []*http.Cookie{user.sessionToken}, 0, user.numFavorites)
		galleriesProducer := make(chan *Gallery, 20)
		go getGalleries(channel, galleriesProducer)
		for val := range galleriesProducer {
			user.favorites = append(user.favorites, val)
		}
	}
	// Copy so that the user isn't able to modify *our* slice
	returnSlice := make([]*Gallery, len(user.favorites))
	copy(returnSlice, user.favorites)
	return returnSlice, nil
}
