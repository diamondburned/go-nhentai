package nhentai

import (
	// "encoding/json"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
	"sync"

	"gitlab.com/diamondburned/go-nhentai/utils"
)

type GalleryTitle struct {
	English  string `json:"english"`
	Japanese string `json:"japanese"`
	Pretty   string `json:"pretty"`
}

// Gallery is the JSON for one result
type Gallery struct {
	ID        interface{}  `json:"id"`
	MediaID   string       `json:"media_id"`
	Pages     int          `json:"num_pages"`
	Favorites int          `json:"num_favorites"`
	DateUnix  int          `json:"upload_date"`
	Scanlator string       `json:"scanlator"`
	Title     GalleryTitle `json:"title"`
	Images    struct {
		Pages     []Image `json:"pages"`
		Cover     Image   `json:"cover"`
		Thumbnail Image   `json:"thumbnail"`
	} `json:"images"`
	Tags []Tag `json:"tags"`
}

// Image is the struct for each image
type Image struct {
	Parent    *Gallery
	Index     int
	Imagetype string `json:"t"`
	Width     int    `json:"w"`
	Height    int    `json:"h"`
}

// Tag is the struct for each tag
type Tag struct {
	ID    int     `json:"id"`
	Type  TagType `json:"type"`
	Name  string  `json:"name"`
	URL   string  `json:"url"`
	Count int     `json:"count"`
}

// TagType is a const for all possible tagtypes
type TagType = string

// Bla bla
const (
	ArtistTag    TagType = "artist"
	CategoryTag  TagType = "category"
	CharacterTag TagType = "character"
	GroupTag     TagType = "group"
	LanguageTag  TagType = "language"
	ParodyTag    TagType = "parody"
	TagTag       TagType = "tag"
)

// // Similar returns the list of related galleries
// func (gallery *Gallery) Similar() ([]Gallery, error) {
// 	body, err := response(fmt.Sprintf("https://nhentai.net/api/gallery/%v/related", gallery.ID))
// 	if err != nil {
// 		return nil, err
// 	}

// 	results := make([]Gallery, 0, 4)
// 	if err = json.Unmarshal(body, results); err != nil {
// 		return nil, errors.New("nhentai returned invalid json: " + err.Error())
// 	}

// 	for i := 0; i < len(results); i++ {
// 		populateImages(&results[i])
// 	}

// 	return results, nil
// }

// NewGalleryString takes in a Gallery ID string, queries the API and returns a Gallery struct
func NewGalleryString(id string) (*Gallery, error) {
	result, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		return nil, err
	}
	return NewGallery(uint64(result))
}

// NewGallery takes in a Gallery ID, queries the API and returns a Gallery struct
func NewGallery(id uint64) (*Gallery, error) {
	result, err := getGallery(id)
	if err != nil {
		return nil, err
	}
	populateImages(result)

	return result, nil
}

// Download is the simple function to download all pages
func (gallery *Gallery) Download(dest string) error {
	dest = dest + strings.Replace(gallery.Title.English, "/", "%2F", -1) + "/"
	os.MkdirAll(dest, os.ModePerm)

	for _, image := range gallery.Images.Pages {
		if err := image.Download(dest); err != nil {
			os.RemoveAll(dest)
			return err
		}

	}

	return nil
}

// DownloadParallel is the recommended download function which has multithreading
func (gallery *Gallery) DownloadParallel(dest string, retries, threads int) error {
	dest = dest + strings.Replace(gallery.Title.English, "/", "%2F", -1) + "/"
	if err := os.MkdirAll(dest, os.ModePerm); err != nil {
		return err
	}

	var wait sync.WaitGroup
	out := make(chan int, threads*2)

	for i := 0; i < threads; i++ {
		wait.Add(1)

		go func() {
			defer wait.Done()

			for index := range out {
				for i := 1; i < retries+1; i++ {
					if err := gallery.Images.Pages[index].Download(dest); err != nil {
						// TODO(ym): This function shouldn't print anything
						if i == retries {
							utils.DebugPrint(6, fmt.Sprintf("All %v attempts failed. Skipping page %v.", retries, i))
						} else {
							utils.DebugPrint(5, fmt.Sprintf("Download failed on item %v! Retrying attempt %v/%v.", index, i, retries))
						}
					} else {
						break
					}
				}
			}
		}()
	}

	for i := 0; i < gallery.Pages; i++ {
		out <- i
	}

	close(out)
	wait.Wait()
	return nil
}

// URL returns the url of the image
func (image *Image) URL() string {
	imagetype, _ := image.Type()
	return fmt.Sprintf("https://webseed.nhentai.net/galleries/%v/%v.%s", image.Parent.MediaID, image.Index+1, imagetype)
}

// Download downloads the image
func (image Image) Download(dest string) error {
	padding := strconv.Itoa(len(strconv.Itoa(len(image.Parent.Images.Pages))))
	imagetype, err := image.Type()
	if err != nil {
		return err
	}

	filename := fmt.Sprintf("%0"+padding+"d", image.Index+1) + "." + imagetype
	utils.DebugPrint(3, fmt.Sprintf("Fetching: %s / Page %v/%v", image.URL(), image.Index+1, image.Parent.Pages), "Fetching to: "+dest+filename)
	return utils.DownloadFile(dest+filename, image.URL())
}

var fileFormat = map[string]string{"p": "png", "j": "jpg"}

// Type is the function to expand image formats
func (image *Image) Type() (string, error) {
	if val, ok := fileFormat[image.Imagetype]; ok {
		return val, nil
	}
	return "", errors.New("Unknown file format")
}
