package nhentai

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"sync"

	"github.com/anaskhan96/soup"
)

// TODO(ym): Remove soup? maybe?
var (
	ErrNoPage = errors.New("No pages")
)

/* SAMPLE HTML
<div class="gallery" data-tags="1033 1207 1590 2820 2937 5200 5357 5529 5620 7142 7256 7995 8010 8643 8653 8739 9083 9406 9990 10811 11376 12227 12824 13722 13989 14283 14971 15347 15408 15658 15782 15853 16236 17249 20035 20284 21989 22942 22945 23237 23895 24201 24984 25601 25766 27384 27553 27697 29023 29182 29859 30126 32341 32589 32602 32870 33173 36957">
	<a href="/g/215081/" class="cover" style="padding:0 0 83.6% 0">
		<img is="lazyload-image" class="lazyload" width="250" height="209" data-src="https://t.nhentai.net/galleries/1144747/thumb.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />
		<noscript>
			<img src="https://t.nhentai.net/galleries/1144747/thumb.jpg" width="250" height="209" />
		</noscript>
		<div class="caption">
			[Fuusen Club] Haha Mamire [English] [LWB, csynn, SaHa]
		</div>
	</a>
</div>
*/

// TODO(ym): Maybe these should return channels? now that we are scraping these can be quite slow (specially for large queries)
// TODO(ym): Errors
func Search(query string, start, end int) ([]*Gallery, error) {
	galleriesProducer := make(chan *Gallery, 20)
	pageProducer, pageStart, pageLimit := realSearch("https://nhentai.net/search/?q="+url.QueryEscape(query), nil, start, end)
	go getGalleries(pageProducer, galleriesProducer)

	results := make([]*Gallery, 0, (pageLimit-pageStart)*25)
	for i := range galleriesProducer {
		// TODO(ym): hack for now, proper error handling
		if i != nil {
			results = append(results, i)
		}
	}

	startNum := start - (pageStart * 25)
	results = results[startNum : end+startNum]
	return results, nil
}

func realSearch(query string, cookies []*http.Cookie, start, end int) (chan uint64, int, int) {
	pageStart := int(math.Floor(float64(start) / 25.0))
	pageLimit := int(math.Ceil(float64(end+start) / 25.0))
	pageResults := make(chan uint64, 20)
	go getPages(query, cookies, pageStart, pageLimit, pageResults)
	return pageResults, pageStart, pageLimit
}

func getPages(query string, cookies []*http.Cookie, start, end int, consumer chan uint64) {
	perRoutine := int(math.Ceil(float64(end-start) / 1.0))
	var waitgroup sync.WaitGroup

	for i := 0; i < 1; i++ {
		waitgroup.Add(1)
		go func(i int) {
			for j := perRoutine * i; j < perRoutine*(i+1); j++ {
				if j > end {
					break
				}
				results, err := getPage(query, cookies, j+1)
				// TODO/NOTE(ym): Excellent error handling
				if err != nil {
					log.Println("Failed to get page: " + strconv.Itoa(j))
					continue
				}
				for _, val := range results {
					consumer <- val
				}
			}
			waitgroup.Done()
		}(i)
	}

	waitgroup.Wait()
	close(consumer)
}

func getGalleries(producer chan uint64, consumer chan *Gallery) {
	var waitgroup sync.WaitGroup
	for i := 0; i < 1; i++ {
		waitgroup.Add(1)
		go func() {
			for id := range producer {
				gallery, err := getGallery(id)
				// TODO/NOTE(ym): Excellent error handling
				if err != nil {
					log.Println("Failed to get gallery:" + strconv.FormatUint(id, 10) + ", " + err.Error())
					continue
				}
				populateImages(gallery)
				consumer <- gallery
			}
			waitgroup.Done()
		}()
	}
	waitgroup.Wait()
	close(consumer)
}

// SearchGallery crawls the homepage for galleries
func getPage(query string, cookies []*http.Cookie, page int) ([]uint64, error) {
	if page < 1 {
		return nil, errors.New("number of pages can't be < 0")
	}

	results := make([]uint64, 0, 25) // each page contains 25 entries ALWAYS
	req, err := http.NewRequest("GET", query+"&page="+strconv.Itoa(page), nil)
	for _, val := range cookies {
		req.AddCookie(val)
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	galleries := soup.HTMLParse(string(body)).FindAll("div", "class", "gallery")
	for _, gallery := range galleries {
		// Get the gallery ID from <a href="">
		if galleryID, ok := gallery.Find("a", "class", "cover").Attrs()["href"]; ok {
			IDint, err := strconv.ParseInt(strings.Split(galleryID, "/")[2], 10, 64)
			if err != nil {
				return results, err
			}
			results = append(results, uint64(IDint))
		}
	}

	return results, nil
}

func getGallery(id uint64) (*Gallery, error) {
	body, err := soup.Get("https://nhentai.net/g/" + strconv.FormatUint(id, 10))
	if err != nil {
		return nil, err
	}

	html := soup.HTMLParse(body)

	gallery := new(Gallery)
	var data []byte
	for _, script := range html.Find("body").FindAll("script") {
		fulltext := script.FullText()
		if strings.Contains(fulltext, "var gallery = new N.gallery") {
			data = []byte(regexp.MustCompile(`{.*}`).FindString(strings.Split(fulltext, "\n")[1]))
		}
	}

	if len(data) == 0 {
		fmt.Printf("Body: %s, Data: %s\n", body, data)
		return nil, ErrNoPage
	}

	if err := json.Unmarshal(data, gallery); err != nil {
		return nil, err
	}

	return gallery, nil
}
