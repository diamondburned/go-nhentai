package main

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"
	"os/user"
)

// Config is the struct for config file and database
// location and name are Deliberately Unexported
// TODO(ym): Do we really need both location and name instead of just a path to a file?
type Config struct {
	Query            []string        `json:"query"`       // List of queries you want the bot to check
	Whitelist        []string        `json:"whitelist"`   // List of tags to always download if found
	Blacklist        []string        `json:"blacklist"`   // List of tags to ignore if found
	MaxPages         int             `json:"maxpages"`    // The max pages a doujin could have before skipping
	FetchMax         int             `json:"fetchmax"`    // Number of doujin to fetch per query
	DownloadLocation string          `json:"location"`    // The targeted folder to download to
	APIendpoint      string          `json:"apiendpoint"` // The endpoint the API listens to
	APIhash          string          `json:"apihash"`     // SHA512-encrypted hash for ws
	Fetched          []string        `json:"fetched"`     // The array of fetched doujin (MediaID)
	FetchedIDs       []int64         `json:"fetched_ids"` // .
	WhitelistMap     map[string]bool `json:"-"`           // Map from Whitelisted tags to booleans
	BlacklistMap     map[string]bool `json:"-"`           // Map from Blacklisted tags to booleans
	location         string          // Location of config file
	name             string          // Name of the config file
}

// DefaultConfig is to generate a default config
func DefaultConfig() *Config {
	usr, _ := user.Current()
	return &Config{
		Query:            []string{"english"},
		Blacklist:        []string{"scat"},
		Whitelist:        []string{"english"},
		BlacklistMap:     map[string]bool{"scat": true},
		WhitelistMap:     map[string]bool{"english": true},
		DownloadLocation: usr.HomeDir + "/Downloads/go-nhentai/",
		FetchMax:         15,
		MaxPages:         -1,
		APIendpoint:      ":8096",
		APIhash:          "",
		name:             "config.json",
		location:         usr.HomeDir + "/.config/go-nhentai/"}
}

// NewConfig loads the entire config file and do tests
func NewConfig(location, name string) (*Config, error) {
	var config = Config{location: location, name: name}
	err := config.read()
	if err != nil {
		return nil, err
	}

	// TODO(ym): Move this somewhere else.
	if config.DownloadLocation == "" {
		return nil, errors.New("Download Location not configured")
	}

	config.WhitelistMap = make(map[string]bool)
	for _, v := range config.Whitelist {
		config.WhitelistMap[v] = true
	}

	config.BlacklistMap = make(map[string]bool)
	for _, v := range config.Blacklist {
		config.BlacklistMap[v] = true
	}

	return &config, nil
}

// Write writes the config file
func (config *Config) Write() error {
	json, err := json.MarshalIndent(config, "", "\t")
	if err != nil {
		return err
	}

	if err := os.MkdirAll(config.location, os.ModePerm); err != nil {
		return err
	}

	if err := ioutil.WriteFile(config.location+config.name, json, os.ModePerm); err != nil {
		return err
	}
	return nil
}

// read reads the config file
func (config *Config) read() error {
	data, err := ioutil.ReadFile(config.location + config.name)
	if err != nil {
		return err
	}
	if err := json.Unmarshal(data, &config); err != nil {
		return err
	}
	return nil
}
