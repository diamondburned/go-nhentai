package main

import (
	"fmt"

	"gitlab.com/diamondburned/go-nhentai/nhentai"
	"gitlab.com/diamondburned/go-nhentai/utils"
)

func update(config *Config, force bool, threads int) {
	for _, query := range config.Query {
		if query == "" {
			continue
		}

		results, _ := nhentai.Search(query, 0, config.FetchMax)
		// if len(err) != 0 {
		// 	// TODO(ym): print out all the errors
		// 	utils.DebugPrint(0, "Failed to update galleries for query: "+query+", because: "+err[0].Error()+".")
		// continue
		// }
		for _, gallery := range results {
			GetGallery(gallery, config, force, threads)
		}
	}
}

// Get downloads galleries from command gallery
// Probably shouldn't print anything? idk
func GetGallery(gallery *nhentai.Gallery, config *Config, force bool, threads int) {
	utils.DebugPrint(1, "Checking gallery with Media ID: "+gallery.MediaID)
	if skip, reason := skipGallery(config, force, gallery); skip {
		utils.DebugPrint(2, "Skipping gallery "+gallery.MediaID+": "+reason)
		return
	}

	gallery.DownloadParallel(config.DownloadLocation, 5, threads)

	config.Fetched = append(config.Fetched, gallery.MediaID)
	if err := config.Write(); err != nil {
		utils.DebugPrint(5, "Failed to write config: "+err.Error())
	}
}

func skipGallery(config *Config, force bool, gallery *nhentai.Gallery) (bool, string) {
	if config.MaxPages != -1 && config.MaxPages < gallery.Pages {
		return true, fmt.Sprintf("exceeds max number of pages (%v > %v)", gallery.Pages, config.MaxPages)
	}

	if utils.StrInArray(gallery.MediaID, config.Fetched) {
		if force {
			return false, "gallery already exists but downloading anyways."
		}
		return true, "gallery already exists"
	}

	if status, _ := tagInList(gallery.Tags, config.WhitelistMap); status {
		return false, ""
	}

	if status, tag := tagInList(gallery.Tags, config.BlacklistMap); status {
		return true, "contains blacklisted tag: " + tag
	}

	return false, ""
}

func tagInList(tags []nhentai.Tag, list map[string]bool) (bool, string) {
	for _, tag := range tags {
		if _, avail := list[tag.Name]; avail {
			return true, tag.Name
		}
	}

	return false, ""
}
