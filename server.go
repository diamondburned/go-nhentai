package main

import (
	"encoding/json"
	"go/build"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
)

// Gallery is the struct to push to webserver
type Gallery struct {
	Name    string   `json:"name"`
	ModTime int64    `json:"mod_time"`
	Size    int64    `json:"size"`
	Pages   []string `json:"pages"`
}

// ToStructure converts the local gallery to Structure
func ToStructure(folder string) []Gallery {
	folders, err := ioutil.ReadDir(folder)
	if err != nil {
		println("Couldn't scan folder " + folder)
		log.Fatal(err)
	}

	// REEE why go doesn't have map and reduce
	// Oh right generics :(.
	galleries := make([]Gallery, 0, len(folders))
	for _, folder := range folders {
		galleries = append(galleries, Gallery{Name: folder.Name(), ModTime: folder.ModTime().Unix(), Size: folder.Size()})
	}

	for i, gallery := range galleries {
		files, err := ioutil.ReadDir(folder + gallery.Name + "/")
		if err != nil {
			log.Println("Failed to process gallery " + gallery.Name + ": " + err.Error())
			continue
		}

		pages := make([]string, 0, len(files))
		for _, page := range files {
			pages = append(pages, page.Name())
		}

		// Not really needed due to the way we download files now
		sort.Slice(pages, func(a, b int) bool {
			int1, err1 := strconv.Atoi(strings.Split(pages[a], ".")[0])
			int2, err2 := strconv.Atoi(strings.Split(pages[b], ".")[0])
			if err1 != nil || err2 != nil {
				return false
			}

			return int1 < int2
		})

		galleries[i].Pages = pages
	}

	// Sort the folders, latest first
	sort.Slice(galleries, func(a, b int) bool { return galleries[b].ModTime < galleries[a].ModTime })
	return galleries
}

type DoujinServer struct {
	Location string // Location for gallery
	Hash     string // Hash for the password (sha512sum)
	Addr     string
}

func (server *DoujinServer) ServeHTTP(writer http.ResponseWriter, response *http.Request) {
	if server.Hash != "" && server.Hash != response.Header.Get("Hash") {
		writer.Header().Set("Error", "Incorrect Password")
		http.Error(writer, "Incorrect Password", http.StatusUnauthorized)
		return
	}

	writer.Header().Set("Content-Type", "application/json")
	writer.Header().Set("Access-Control-Allow-Origin", "*")

	// TODO(ym): Cache ToStructure, and poll the dir every so often (5 mins or so)/have some sort of notification when when update downloads a gallery.
	if err := json.NewEncoder(writer).Encode(ToStructure(server.Location)); err != nil {
		log.Println(err)
		writer.Header().Set("Error", err.Error())
		http.Error(writer, err.Error(), http.StatusInternalServerError)
	}
}

// Start starts the webserver
func (server *DoujinServer) Start(assetsPath string) {
	http.Handle("/api/", server)
	http.Handle("/img/", http.StripPrefix("/img/", http.FileServer(http.Dir(server.Location))))
	http.Handle("/", http.FileServer(http.Dir(assetsPath)))

	println("Initiating connection on", server.Addr)
	log.Fatal(http.ListenAndServe(server.Addr, nil))
}

// DefaultAssetsPath returns the default path for the assets
func DefaultAssetsPath() string {
	assetPath := os.Getenv("GOPATH")
	if assetPath == "" {
		assetPath = build.Default.GOPATH
	}
	return filepath.Join(assetPath, "src/gitlab.com/diamondburned/go-nhentai/public")
}
