package utils

import (
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"unicode"
)

// DebugPrint prints if the debug flag is there
func DebugPrint(indent int, strs ...string) {
	indentStr := strings.Repeat("  ", indent)
	var str string

	for _, current := range strs {
		str += indentStr + current + "\n"
		indentStr += "  "
	}

	str = str[:len(str)-1]

	log.Println(str)
}

// DownloadFile downloads a file to a particular location
func DownloadFile(filepath string, url string) error {
	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// To avoid ending up with half finished files
	buffer, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	if err = ioutil.WriteFile(filepath, buffer, os.ModePerm); err != nil {
		return err
	}
	return nil
}

// StrInArray checks if a string is in an array of strings
func StrInArray(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

// IsNumeric Checks if a string contains *only* integers, doesn't check for negatives ("-10" will return false)
func IsNumeric(a string) bool {
	for _, char := range a {
		if !unicode.IsNumber(char) {
			return false
		}
	}
	return true
}

func GetCookie(cookies []*http.Cookie, name string) *http.Cookie {
	for _, cookie := range cookies {
		if cookie.Name == name {
			return cookie
		}
	}
	return nil
}
