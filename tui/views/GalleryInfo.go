package views

import (
	"fmt"
	"sort"
	"strconv"
	"strings"

	"github.com/gdamore/tcell"
	"gitlab.com/diamondburned/go-nhentai/nhentai"
	"gitlab.com/diamondburned/go-nhentai/tui/buffer"
)

type GalleryInfo struct {
	Gallery *nhentai.Gallery
}

func (view *GalleryInfo) UpdateAndRedraw(buf buffer.Buffer) {
	if view.Gallery == nil {
		return
	}
	buf.Clear()

	buf.DrawString(0, 0, "Title: "+view.Gallery.Title.Pretty, tcell.StyleDefault)
	buf.DrawString(len("Title: "), 1, view.Gallery.Title.English, tcell.StyleDefault)
	buf.DrawString(len("Title: "), 2, view.Gallery.Title.Japanese, tcell.StyleDefault)
	buf.DrawString(0, 3, "Pages: "+strconv.Itoa(view.Gallery.Pages), tcell.StyleDefault)
	buf.DrawString(0, 4, "Favourites: "+strconv.Itoa(view.Gallery.Favorites), tcell.StyleDefault)
	buf.DrawString(0, 5, "Scanlator: "+view.Gallery.Scanlator, tcell.StyleDefault)
	buf.DrawString(0, 6, "ID: "+fmt.Sprint(view.Gallery.ID), tcell.StyleDefault)
	buf.DrawString(0, 7, "MediaID: "+view.Gallery.MediaID, tcell.StyleDefault)

	artists := make([]string, 0)
	categorys := make([]string, 0)
	groups := make([]string, 0)
	language := make([]string, 0)
	parody := make([]string, 0)
	tags := make([]string, 0)
	for _, tag := range view.Gallery.Tags {
		switch tag.Type {
		case nhentai.ArtistTag:
			artists = append(artists, tag.Name)
		case nhentai.CategoryTag:
			categorys = append(categorys, tag.Name)
		case nhentai.GroupTag:
			groups = append(groups, tag.Name)
		case nhentai.LanguageTag:
			language = append(language, tag.Name)
		case nhentai.ParodyTag:
			parody = append(parody, tag.Name)
		case nhentai.TagTag:
			tags = append(tags, tag.Name)
			// default:
			// 	panic("Whatever")
		}
	}
	sort.Strings(language)
	buf.DrawString(0, 8, "Languages: "+strings.Join(language, ", "), tcell.StyleDefault)
	sort.Strings(tags)
	buf.DrawString(0, 9, "Tags: ", tcell.StyleDefault)
}

// func tabulate(strings []string, sep rune, maxLength int) []string {
// 	for true {
// 		len := 0
// 		for i < maxLength
// }
// }

// func (view *GalleryInfo) drawString(buf buffer.Buffer, x, y int, input string, style tcell.Style) {
// 	name := []rune(input)
// 	for i := 0; i < len(name); i++ {
// 		buf.Set(x+i, y, name[i], style)
// 	}
// }

func (view *GalleryInfo) SetGallery(gallery *nhentai.Gallery) {
	view.Gallery = gallery
}

func (view *GalleryInfo) Handle(buf buffer.Buffer, ev tcell.Event) {
	// No-op
}
