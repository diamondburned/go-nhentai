package views

import (
	"github.com/gdamore/tcell"
	"gitlab.com/diamondburned/go-nhentai/tui/buffer"
)

type BorderView struct {
	child buffer.View
}

// TODO(ym): make cleaner using mafs (slope to get the char and pythagorean theorem or something)
func DrawBorder(buf buffer.Buffer) {
	length, width := buf.Size()
	style := tcell.StyleDefault.Foreground(tcell.ColorDimGray)
	for i := 0; i < length; i++ {
		buf.Set(0, i, '│', style)
	}
	for i := 0; i < width; i++ {
		buf.Set(i, 0, '─', style)
	}
	for i := 0; i < width; i++ {
		buf.Set(i, length, '─', style)
	}
	for i := 0; i < length; i++ {
		buf.Set(width, i, '│', style)
	}
	buf.Set(0, 0, '┌', style)
	buf.Set(width, 0, '┐', style)
	buf.Set(0, length, '└', style)
	buf.Set(width, length, '┘', style)
}

func (view BorderView) UpdateAndRedraw(buf buffer.Buffer) {
	DrawBorder(buf)
	// TODO(ym):
	// view.child.Draw(buf)
}

func (view BorderView) Handle(buf buffer.Buffer, event tcell.Event) {
	view.child.Handle(buf, event)
}
