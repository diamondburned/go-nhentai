package views

// Should be renamed scroll view tbh
import (
	"github.com/gdamore/tcell"
	"gitlab.com/diamondburned/go-nhentai/tui/buffer"
)

type KeyHandler func(*GalleriesView, []interface{}, int)
type Renderer func(int, int, int, []interface{}) string

type GalleriesView struct {
	SelectedHandler    *KeyHandler
	render             *Renderer
	onEndReached       *func(*GalleriesView)
	onSelectionChanged *KeyHandler
	keybindHandlers    map[rune]KeyHandler
	items              []interface{}
	head, selected     int
	shouldCallEnd      bool
}

func (view *GalleriesView) CharHandler(key rune, handler KeyHandler) {
	view.keybindHandlers[key] = handler
}

func (view *GalleriesView) Clear() {
	view.items = view.items[:0]
}

func (view *GalleriesView) OnEndReached(handler func(*GalleriesView)) {
	view.onEndReached = &handler
}

func NewGalleriesView(SelectedHandler KeyHandler, render Renderer) *GalleriesView {
	return &GalleriesView{SelectedHandler: &SelectedHandler, render: &render, keybindHandlers: make(map[rune]KeyHandler), shouldCallEnd: true}
}

func (view *GalleriesView) Length() int {
	return len(view.items)
}

func (view *GalleriesView) AddItem(item interface{}) {
	view.shouldCallEnd = true
	view.items = append(view.items, item)
}

func (view *GalleriesView) OnSelectionChanged(handler KeyHandler) {
	view.onSelectionChanged = &handler
}

func (view *GalleriesView) UpdateAndRedraw(buf buffer.Buffer) {
	buf.Clear()
	out := ""
	length, width := buf.Size()
	if view.head+length > len(view.items)-view.head && view.shouldCallEnd && view.onEndReached != nil {
		// TODO(ym): Use a context
		go (*view.onEndReached)(view)
		view.shouldCallEnd = false
	}
	// buf.DrawBorder("Galleries")
	style := tcell.StyleDefault.Underline(true)
	unselectedStyle := tcell.StyleDefault
	for i := view.head; i < len(view.items); i++ {
		render := (*view.render)(i, 1, width-3, view.items)
		if i == view.selected {
			buf.DrawString(0, i-view.head, render, style)
		} else {
			buf.DrawString(0, i-view.head, render, unselectedStyle)
		}
	}
	buf.DrawString(0, 0, out, tcell.StyleDefault)
}

func (view *GalleriesView) Handle(buf buffer.Buffer, ev tcell.Event) {
	switch ev := ev.(type) {
	case *tcell.EventKey:
		switch ev.Key() {
		case tcell.KeyEnter:
			(*view.SelectedHandler)(view, view.items, view.selected)
		case tcell.KeyRune:
			length, _ := buf.Size()
			if len(view.items) != 0 {
				switch ev.Rune() {
				case 'u':
					view.head -= length
					if view.head < 0 {
						view.head = 0
					}
					view.selected = view.head
				case 'd':
					view.head += length
					if view.head > len(view.items) {
						view.head = len(view.items) - (len(view.items) % length)
					}
					view.selected = view.head
				case 'g':
					view.selected = 0
					view.head = 0
				case 'G':
					view.selected = len(view.items) - 1
					view.head = (view.selected / length) * length
				case 'j':
					view.selected = (view.selected + 1) % len(view.items)
					view.head = (view.selected / length) * length
				case 'k':
					view.selected = ((view.selected - 1) + len(view.items)) % len(view.items)
					view.head = (view.selected / length) * length
				default:
					if !(view.keybindHandlers[ev.Rune()] == nil) {
						handler := view.keybindHandlers[ev.Rune()]
						handler(view, view.items, view.selected)
					}
				}
			}
		}
		if view.onSelectionChanged != nil {
			go (*view.onSelectionChanged)(view, view.items, view.selected)
		}
	}
}
