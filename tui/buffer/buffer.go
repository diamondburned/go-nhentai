package buffer

import (
	"github.com/gdamore/tcell"
)

// New(Buffer, int, int, int, int, string) Buffer
// TODO(ym):
type Buffer interface {
	Clear()
	Set(int, int, rune, tcell.Style)
	Get(int, int) (rune, tcell.Style)
	Size() (int, int)
	// TODO(ym): Hmm probably shouldn't be here
	Invalidate()
	Invalid() bool
	DrawString(int, int, string, tcell.Style)
}

// View does stuff
// Should probably be a Change to stuff like KeyHandler, MouseHandler, and stuff like that.
type View interface {
	Handle(Buffer, tcell.Event)
	UpdateAndRedraw(*Buffer)
}

type Rect struct {
	Buffer
	x, y, length, width int
	invalid             bool
	name                string
}

func New(buf Buffer, x, y, length, width int, name string) *Rect {
	rect := Rect{buf, x, y, length, width, false, name}
	return &rect
}

func (rect *Rect) Set(x, y int, char rune, style tcell.Style) {
	if rect.check(x, y) {
		return
	}
	rect.Buffer.Set(rect.x+x, rect.y+y, char, style)
}

func (rect *Rect) check(x, y int) bool {
	return (x >= rect.width || y >= rect.length) || rect.invalid
}

func (rect *Rect) Get(x, y int) (rune, tcell.Style) {
	if rect.check(x, y) {
		return 0, tcell.StyleDefault
	}
	result, style := rect.Buffer.Get(rect.x+x, rect.y+y)
	return result, style
}

func (rect *Rect) Size() (int, int) {
	return rect.length - 1, rect.width - 1
}

func (rect *Rect) Invalidate() {
	rect.invalid = true
}

func (rect *Rect) Invalid() bool {
	return rect.invalid
}
