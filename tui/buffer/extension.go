package buffer

import (
	"strings"

	"github.com/gdamore/tcell"
)

// TODO(ym): Check if embedding will break stuff (aka encapsulation) // write a better comment lmao
type Screen struct {
	tcell.Screen
}

func (screen *Screen) Set(x, y int, char rune, style tcell.Style) {
	screen.SetContent(x, y, char, nil, style)
}

func (screen *Screen) Get(x, y int) (rune, tcell.Style) {
	result, _, style, _ := screen.GetContent(x, y)
	return result, style
}

func (screen *Screen) Clear() {
	if screen.Invalid() {
		return
	}
	length, width := screen.Size()
	for i := 0; i < length; i++ {
		for j := 0; j < width; j++ {
			screen.Set(j, i, ' ', tcell.StyleDefault)
		}
	}
}

// TODO(ym):
// Maybe these should be no-op?
func (screen *Screen) Invalidate() {
}
func (screen *Screen) Invalid() bool {
	return false
}

// TODO(ym): Add a wrap boolean
// TODO(ym): Remove the reciever
func (screen *Screen) DrawString(x, y int, item string, style tcell.Style) {
	length, _ := screen.Size()
	for _, s := range strings.Split(item, "\n") {
		for i, c := range s {
			screen.Set(x+i, y, c, style)
		}
		y++
		if y == length {
			break
		}
	}
}
