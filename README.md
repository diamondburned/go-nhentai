# go-nhentai

A nhentai wrapper in Go

## Uses

- Automatically fetch new doujins and requested ones
- API wrapper (to-do)
- CLI front-end (to-do)
- Web front-end

### Automatically fetching doujins

Throw the command in crontab, and you're set.

## Manual

```bash
man $GOPATH/src/gitlab.com/diamondburned/go-nhentai/doc/go-nhentai.1

# OR

./go-nhentai -help
```

## `config.json` docs

| Key           | Type       | Description                                       |
| ------------- | ---------- | ------------------------------------------------- |
| `query`       | `[]string` | List of queries you want the bot to check         |
| `whitelist`   | `[]string` | List of tags to always download if found          |
| `blacklist`   | `[]string` | List of tags to ignore if found                   |
| `maxpages`    | `int`      | The max pages a doujin could have before skipping |
| `fetchmax`    | `int`      | Number of doujin to fetch per query               |
| `location`    | `string`   | The targeted folder to download to                |
| `apiendpoint` | `string`   | The endpoint the API listens to                   |
| `apihash`     | `string`   | SHA512-encrypted hash for the webserver           |
| `fetched`     | `[]string` | The array of fetched doujin (don't touch this)    |

_** To generate `APIhash`, run `printf "your password here" | sha512sum` then copy everything before the `-`_

## Arguments

- `./main.go update` updates the local library
- `./main.go ws` runs the webserver

## Web front-end information

- The `go-nhentai` web front-end hosts your downloaded doujins on a simple web interface
- By default, `go-nhentai` listens to `:8096` and has no passwords
- If you haven't changed anything yet, type `IP:8096/api` into the input bar
	- If you're on the same machine, `localhost:8096/api` will do
- CSS framework is Bulma
- Icons sets are from `material.io`

## Build instructions

*Caution:* You need `go` for installing this, even just once. `go` manages fetching frontend assets right now, which is why it's mandatory.

```bash
# Make sure you have Go

# Set a GOPATH if you haven't
[ "$GOPATH" ] && export GOPATH="$HOME/go"
# Throw that into ~/.shellrc while you're at it

# Install go-nhentai
go get -u gitlab.com/diamondburned/go-nhentai

# Run it to initiate the config
$GOPATH/bin/go-nhentai update

# Optional
# Add PATH into your ~/.shellrc
export PATH="$GOPATH/bin:$PATH"
```

## Run flags

- `--silent` prints nothing except errors
- `--threads [n]` will download `n` pages at once, by default `n` will be the amount of logical cores you have.

## To-do

- [ ] Database to keep track of metadata
- [ ] Image preloading and progress bar
- [ ] Loading animation
- [ ] API wrapper
- [ ] Booru support
- [ ] Remote command to update library, maybe even with live progress
- [ ] Add in sorting methods and API handlers
- [ ] CLI front-end (assigned to ym)
- [x] Favorites
- [x] Add in a function to manually download a doujin into the library (top priority)
- [x] Add in multithreaded downloading (configurable) (thanks ym)
- [x] Added whitelisting for all your fetish needs
- [x] Password verification for front-end (hashing client-side since no SSL)
- [x] Go back to `encoding/json` instead of `json-iterator`
- [x] Web front-end that I thought up of on a whim

